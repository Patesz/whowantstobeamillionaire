﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WhoWantsToBeAMillionaire;

namespace WhoWantsToBeAMillionaireTest
{
    [TestClass]
    public class QuizQuestionTest
    {
        [TestMethod]
        public void Test_ReadCorrect()
        {
            string valid = "Question, Ans1, Ans2, Ans3, Ans4, 1";
            string valid2 = "Mi az ABC első betűje?, x, a, y, b, 2";
            string valid3 = "Mi az ABC első betűje?, x, a, y, b, 2";
            string valid4 = "'Quotes 'in' quotes.', Ans1, 'Ans2, Ans3, Ans4', Ans5, Ans6, 1"; 

            string empty = "";
            string invalid_NumberLow = "Question, Ans1, Ans2, Ans3, Ans4, 0";
            string invalid_NumberUp = "Question, Ans1, Ans2, Ans3, Ans4, 5";
            string invalid_FewComa = "Question, Ans1, Ans3, Ans4, 1";
            string invalid_TooMuchComa = "Question, Ans1, Ans2, Ans3, Ans4, Ans5, 1";

            new QuizQuestion(valid);
            new QuizQuestion(valid2);
            new QuizQuestion(valid3);
            new QuizQuestion(valid4);
            Assert.ThrowsException<QuestionFormatException>(() => new QuizQuestion(empty));
            Assert.ThrowsException<QuestionFormatException>(() => new QuizQuestion(invalid_NumberLow));
            Assert.ThrowsException<QuestionFormatException>(() => new QuizQuestion(invalid_NumberUp));
            Assert.ThrowsException<QuestionFormatException>(() => new QuizQuestion(invalid_FewComa));
            Assert.ThrowsException<QuestionFormatException>(() => new QuizQuestion(invalid_TooMuchComa));

        }
    }
}
