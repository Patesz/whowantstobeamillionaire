﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WhoWantsToBeAMillionaire
{

    public class EmptyFileException : Exception
    {
    }

    public class QuizQuestionReader
    {

        public string QuestionsPath { get; private set; }

        public QuizQuestionReader(string QuestionsPath)
        {
            this.QuestionsPath = QuestionsPath;
        }

        /// <summary>
        /// Reads and returns quiz questions from resources as a QuizQuestion object.
        /// </summary>
        /// <param name="shuffle">Shuffles the order of QuizQuestion objects if true.</param>
        /// <param name="limit">Get this many number of QuitQuestion objects. If less than 0 it returns all objects.</param>
        /// <returns>Enumerable QuizQuestion objects.</returns>
        public List<QuizQuestion> ReadQuestions()
        {
            List<string> lines = File.ReadAllLines(QuestionsPath).ToList();

            lines.RemoveAll(x => string.IsNullOrEmpty(x));

            if (lines.Count == 0)
            {
                throw new EmptyFileException();
            }

            return ToQuizQuestions(lines);
        }

        private List<QuizQuestion> ToQuizQuestions(List<string> lines)
        {
            List<QuizQuestion> res = new List<QuizQuestion>();

            for (int i = 0; i < lines.Count; i++)
            {
                try { 
                    res.Add(new QuizQuestion(lines[i]));
                }
                catch (QuestionFormatException ie)
                {
                    throw new QuestionFormatException($"Mistake in line {i+1}!\n{ie.Message}");
                }
            }

            return res;
        }

    }
}

