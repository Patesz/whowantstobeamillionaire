﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace WhoWantsToBeAMillionaire
{

    public class QuestionFormatException : Exception
    {
        public QuestionFormatException(string message) : base(message) { }
    }

    public class QuizQuestion : System.ICloneable
    {

        public string Question { get; private set; }

        public List<string> Answers { get; private set; } = new List<string>();

        public int CorrectIndex { get; private set; }

        public QuizQuestion(string Question, List<string> Answers, int CorrectIndex)
        {
            this.Question = Question;
            this.Answers = Answers;
            this.CorrectIndex = CorrectIndex;
        }

        public QuizQuestion(string line)
        {
            int correctNumber = -1;

            List<string> split = Regex.Split(line, ",(?=(?:[^']*'[^']*')*[^']*$)").ToList();

            if (split.Count != 6)
            {
                throw new QuestionFormatException("Must be exactly 5 ',' character in a single line!");
            }

            for (int i = 0; i < split.Count; i++)
            {
                if (split[i].StartsWith("'") && split[i].EndsWith("'"))
                {
                    split[i] = split[i].Substring(1, split[i].Length - 2);
                }
            }

            string question = split[0];
            List<string> answers = split.Skip(1).Take(4).ToList();

            try
            {
                correctNumber = int.Parse(split[5]);
                if (correctNumber < 1 || correctNumber > 4)
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
            catch (Exception ex)
            {
                if (ex is FormatException || ex is ArgumentOutOfRangeException || ex is OverflowException)
                {
                    throw new QuestionFormatException("Last value in line must be either 1, 2, 3 or 4!");
                }
                else if (ex is ArgumentNullException)
                {
                    throw new QuestionFormatException("Last value in line is missing!");
                }
            }

            Question = question;
            Answers = answers; 
            CorrectIndex = correctNumber - 1;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        public void ShuffleAnswers()
        {
            string correctAnswer = Answers[CorrectIndex];

            Answers.Shuffle();

            CorrectIndex = Answers.FindIndex(answer => answer == correctAnswer);
        }

        public char GetCorrectLetter()
        {
            return (char)(CorrectIndex.ToString()[0] + 17);
        }

        public string GetCorrectAnswer()
        {
            return Answers[CorrectIndex];
        }
    }
}
