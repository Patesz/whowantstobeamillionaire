﻿using System.Collections.Generic;

namespace WhoWantsToBeAMillionaire
{
    public class Progress
    {
        public int Number { get; private set; }
        public float Prize { get; private set; }

        public Progress(int Number, int Prize)
        {
            this.Number = Number;
            this.Prize = Prize;
        }
    }

    public class ProgressGenerator
    {

        public List<Progress> Generate(int num)
        {
            List<Progress> res = new List<Progress>(num);
            int prize = 0;

            for (int i = 0; i < num; i++)
            {
                if (i < 3)
                {
                    prize += 100;
                }
                else if (i == 3)
                {
                    prize += 200;
                }
                else
                {
                    prize *= 2;
                }

                res.Add(new Progress(i + 1, prize));
            }
            return res;
        }

    }
}
