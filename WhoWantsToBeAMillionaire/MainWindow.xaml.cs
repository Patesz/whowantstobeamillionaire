﻿using System.Collections.Generic;
using System.Windows;
using System.IO;
using System;

namespace WhoWantsToBeAMillionaire
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GameController Controller;

        private static void ShowError(string message)
        {
            MessageBox.Show(
                message, "Error", MessageBoxButton.OK, MessageBoxImage.Error
            );
            Application.Current.Shutdown();
        }

        public MainWindow()
        {
            try
            {
                Controller = new GameController(Directory.GetCurrentDirectory(), "settings.txt", "questions.txt");
                InitializeComponent();

                Progress.ItemsSource = Controller.ProgressList;

                SetQuestion();
            }
            catch (FileNotFoundException)
            {
                ShowError("questions.txt file not found!");
            }
            catch (EmptyFileException)
            {
                ShowError("questions.txt file is empty!");
            }
            catch (QuestionFormatException e)
            {
                ShowError(e.Message);
            }
        }

        private void SetQuestion()
        {
            QuizQuestion q = Controller.GetCurrentQuestion();

            Question.Text = q.Question;
            Option1.Content = $"A: {q.Answers[0]}";
            Option2.Content = $"B: {q.Answers[1]}";
            Option3.Content = $"C: {q.Answers[2]}";
            Option4.Content = $"D: {q.Answers[3]}";

            Progress.SelectedItem = Controller.GetCurrentProgress();
            Progress.Focus();
            UpdateLayout();
        }

        private void Option1_Click(object sender, RoutedEventArgs e)
        {
            OnOptionClick(1);
        }

        private void Option2_Click(object sender, RoutedEventArgs e)
        {
            OnOptionClick(2);
        }

        private void Option3_Click(object sender, RoutedEventArgs e)
        {
            OnOptionClick(3);
        }

        private void Option4_Click(object sender, RoutedEventArgs e)
        {
            OnOptionClick(4);
        }

        private void OnOptionClick(int option)
        {
            AnswerResult res = Controller.CheckAnswer(option);

            if (res == AnswerResult.Win)
            {
                MessageBox.Show(
                    $"Congratulations, you answered all {Controller.ProgressList[0].Number} questions correctly. " +
                    $"You won ${Controller.ProgressList[0].Prize}!", 
                    "Congrats!", MessageBoxButton.OK, MessageBoxImage.Asterisk
                );

                Application.Current.Shutdown();
            }
            else if (res == AnswerResult.Incorrect)
            {
                MessageBox.Show(
                    $"Your guess was wrong. The correct answer was {Controller.GetCurrentQuestion().GetCorrectLetter()}.",
                    "Game over!", MessageBoxButton.OK, MessageBoxImage.Stop
                );

                Application.Current.Shutdown();
            }
            else
            {
                Controller.NextQuestion();
                SetQuestion();
            }
        }
    }
}
