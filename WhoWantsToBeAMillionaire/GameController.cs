﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WhoWantsToBeAMillionaire
{
    public enum AnswerResult
    {
        Correct,
        Incorrect,
        Win
    }

    public class GameController
    {
        private static int QuestionIndex = 0;
        public List<QuizQuestion> QuizQuestions { get; private set; }

        public List<Progress> ProgressList { get; private set; }

        public GameController(string path, string settingsFileName, string questionsFileName)
        {
            string settingsPath = Path.Combine(path, settingsFileName);
            string questionsPath = Path.Combine(path, questionsFileName);

            bool shuffleQuestions = false;
            bool shuffleAnswers = false;
            int maxQuestions = -1;

            try
            {
                List<string> lines = File.ReadAllLines(settingsPath).ToList();
                lines.RemoveAll(x => string.IsNullOrEmpty(x));

                Dictionary<string, string> settings = new Dictionary<string, string>();

                foreach (string line in lines)
                {
                    string[] kv = line.Split('=');
                    settings.Add(kv[0], kv[1]);
                }

                if (settings.ContainsKey("maxQuestions"))
                {
                    maxQuestions = int.Parse(settings["maxQuestions"]);
                }

                if (settings.ContainsKey("shuffleQuestions"))
                {
                    shuffleQuestions = bool.Parse(settings["shuffleQuestions"]);
                }

                if (settings.ContainsKey("shuffleAnswers"))
                {
                    shuffleAnswers = bool.Parse(settings["shuffleAnswers"]);
                }
            } catch (FileNotFoundException) { }
            finally
            {
                QuizQuestions = new QuizQuestionReader(questionsPath).ReadQuestions();

                ProgressList = new ProgressGenerator().Generate(QuizQuestions.Count);
                ProgressList.Reverse();

                if (maxQuestions > 0 && maxQuestions < QuizQuestions.Count)
                {
                    QuizQuestions = QuizQuestions.GetRange(0, maxQuestions);
                }

                if (shuffleAnswers)
                {
                    foreach (QuizQuestion q in QuizQuestions)
                    {
                        q.ShuffleAnswers();
                    }
                }

                if (shuffleQuestions)
                {
                    QuizQuestions.Shuffle();
                }
            }

        }

        public AnswerResult CheckAnswer(int Answer)
        {
            if (Answer == QuizQuestions[QuestionIndex].CorrectIndex + 1)
            {
                if (QuizQuestions.Count - 1 == QuestionIndex)
                {
                    return AnswerResult.Win;
                }
                return AnswerResult.Correct;
            }
            return AnswerResult.Incorrect;
        }

        public void NextQuestion()
        {
            ++QuestionIndex;
        }

        public QuizQuestion GetCurrentQuestion()
        {
            return QuizQuestions[QuestionIndex];
        }

        public Progress GetCurrentProgress()
        {
            int index = ProgressList.Count - QuestionIndex - 1;

            return ProgressList[index];
        }
    }

}
